import { test } from '@playwright/test';
import { DemoLoginPage } from '../page/login_page'
import { PaymentPage } from '../page/KTCK_Tu_payment'


/*
TC1: Verify Payment page shows when clicking on Payment link in menu bar.
TC2: Verify all payment methods shows when opening Payment page
TC3: Verify Credit card page shows when clicking on Credit card in Payment page
TC4: Verify input amount shows correctly when clicking on 200, 500, 1000, 2000, 5000 button
TC5: Verify all fee is calculated correctly when enter valid data into amount input

*/

test.beforeEach ('login', async ({ page }) => {
    const demoLogin = new DemoLoginPage(page);
    await demoLogin.goto();
    await demoLogin.enterData();
    await demoLogin.clickOnLoginButton();
    await demoLogin.verifylogin();
})

test.describe('test payment credit card',async () => {
    test('TC-1 Verify Payment page shows when clicking on Payment link in menu bar', async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.openPaymentPage();
    })
     test.only('TC2: Verify all payment methods shows when opening Payment page',async ({ page }) => {
        const payment = new PaymentPage(page);
        await payment.clickPaymentTab();
        await payment.showAllPaymentMethod();
        
     })
    
})