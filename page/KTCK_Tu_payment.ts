import { Locator, Page, expect } from "@playwright/test"


export class PaymentPage {
    page: Page;
    paymentTab: Locator;
    paymentTitle: Locator;
    ecomdyBalance: Locator;
    visaMethod: Locator;
    paypalMethod: Locator;
    payoneerMethod: Locator;
    wiseMethod: Locator;
    usdtMethod: Locator;
    airwallexMethod: Locator
    tazapayMethod: Locator;
    lianlianMethod: Locator;

    constructor ( page: Page ) {
        this.page = page;
        this.paymentTab = page.getByTestId('tab-layout.textpayment-navbar');
        this.paymentTitle = page.locator('.content-header-title.float-left.pr-1.mb-0.border-right-0');
        this.ecomdyBalance = page.locator('.money.font-medium');
        this.visaMethod = page.locator('#trigger-credit-card');
        this.paypalMethod = page.locator('#trigger-paypal');
        this.payoneerMethod = page.locator('#trigger-payoneer');
        this.wiseMethod = page.locator('#trigger-wise');
        this.usdtMethod = page.locator('#trigger-usdt');
        this.airwallexMethod = page.locator('#trigger-airwallex');
        this.tazapayMethod = page.locator('#trigger-tazapay');
        this.lianlianMethod = page.locator('#trigger-lianlian');
        
    }

    async clickPaymentTab() {
        await this.paymentTab.click();
    }

    async openPaymentPage() {
        await expect(this.paymentTitle).toHaveText('Payment');
        await expect(this.ecomdyBalance).toBeVisible();
    }

    async showAllPaymentMethod() {
        await expect(this.visaMethod).toBeVisible();
        await expect(this.paypalMethod).toContainText(' PayPal ');
        await expect(this.payoneerMethod).toContainText(' payoneer ');
        await expect(this.wiseMethod).toContainText(' WISE ');
        await expect(this.usdtMethod).toContainText(' USDT ');
        await expect(this.airwallexMethod).toContainText(' Airwallex ');
        await expect(this.tazapayMethod).toContainText(' tazapay ');
        await expect(this.lianlianMethod).toContainText(' LianLian ');
    }

}