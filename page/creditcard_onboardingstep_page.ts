import { Locator, Page, expect } from "@playwright/test"
import { log } from "console"

export class CreditCardOnBoadingPage {
    page: Page
    addFundStep: Locator
    balance: Locator
    creditCardTab: Locator
    amount: Locator
    errorMessage: Locator
    topupButton:Locator
    amountInSummary: Locator
    serviceChange: Locator
    creditCardFee: Locator
    tax: Locator
    totalAmount: Locator
    successMessage: Locator
    createAdAccountstep: Locator

    constructor(page:Page) {
        this.page = page;
        this.addFundStep = page.getByTestId('title-step-run-campaign-1');
        this.balance = page.locator('.amount.font-20.font-weight-bold')
        this.creditCardTab = page.locator('#trigger-credit-card-add-fund-child');
        this.amount = page.getByTestId('input-amount-add-fund');
        this.errorMessage = page.locator('#trigger-credit-card-add-fund').locator('#amount').locator('//small');
        this.topupButton = page.getByTestId('trigger-top-up-cc');
        this.amountInSummary = page.locator('.summary-container').getByTestId('amount-credit-card');
        this.serviceChange = page.getByTestId('service-charge-credit-card');
        this.creditCardFee = page.locator('.summary-container').getByTestId('credit-card-fee');
        this.tax = page.getByTestId('tax-fee-credit-card');
        this.totalAmount = page.locator('.summary-container').getByTestId('total-amount-credit-card');
        this.successMessage = page.locator('.toastification-title.text-success');
        this.createAdAccountstep = page.locator('.content-header-title.float-left.pr-1.mb-0.border-right-0');
    }

    // async goto() {
    //     await this.page.goto('https://dev-tiktok.ecomdy.com/');
    // }

    async clickOnAddFundStep() {
        await this.addFundStep.click();
    }
    
    async beforeBalance() {
        const openingBalance = await this.balance.textContent();
        return openingBalance;     
    }

    async clickOnCreditCardTab() {
        await this.creditCardTab.click();
    }

    async enterAmount() {
        await this.amount.fill('200');
    }

    async message() {
        const message = await this.errorMessage.textContent();
        expect(message).toBe(' This amount is suitable ');
    }

    async enableTopUpButton() {
        await expect(this.topupButton.first()).not.toBeDisabled();
    }

    async summary() {
        await expect(this.amountInSummary).toHaveText('200 USD');
        await expect(this.serviceChange).toHaveText('6 USD');
        await expect(this.creditCardFee).toHaveText('8 USD');
        await expect(this.tax).toHaveText('6 USD');
        await expect(this.totalAmount).toHaveText('220 USD');
    }

    async clickOnTopUpButton() {
        await this.topupButton.first().click();
    }

    async topupSuccess() {
        await expect(this.successMessage).toHaveText("Top up success!");
        
    }

    async adAccountstep() {
        await expect(this.createAdAccountstep).toHaveText('Get TikTok Agency Ads Account');
    }
    
    async afterBalance() {
        const closingBalance = await this.balance.textContent();
        return closingBalance;    
    }

    async compareBalances({ beforeBalance, afterBalance }) {
        expect(+afterBalance).toBe(+beforeBalance + 200);
    }

}

