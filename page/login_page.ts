import { Locator, Page, expect } from "@playwright/test"

export class DemoLoginPage {
    page: Page
    email: Locator
    password: Locator
    loginButton:Locator
    userInfo: Locator

    constructor(page:Page) {
        this.page = page
        this.email = page.getByPlaceholder('Enter your email')
        this.password = page.getByPlaceholder('Password')
        this.loginButton = page.getByTestId('btn-login')
        this.userInfo = page.getByTestId('btn-info-user')
    }

    async goto() {
        await this.page.goto('https://dev-tiktok.ecomdy.com/');
    }

    async enterData() {
        await this.email.fill('nclongkk@gmail.com')
        await this.password.fill('12341234')
    }
    async clickOnLoginButton() {
        await this.loginButton.click()
    }

    async verifylogin() {
        await expect(this.userInfo).toBeVisible();
    }
}



