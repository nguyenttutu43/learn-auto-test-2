import {test} from '@playwright/test';
import { DemoLoginPage } from '../page/login_page'

test ('demo login',async ({page}) => {
    const demoLogin = new DemoLoginPage(page)
    await demoLogin.goto()
    await demoLogin.enterData()
    await demoLogin.clickOnLoginButton()
    await demoLogin.verifylogin()
    
}
)
