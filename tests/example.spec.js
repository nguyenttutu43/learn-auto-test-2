      
// // @ts-check
// const { test, expect } = require('@playwright/test');
// const url = 'https://6525608267cfb1e59ce72cb8.mockapi.io/User';

// async function getListUser(request) {
//   const response = await request.get(url, {});
//   expect(response.ok()).toBeTruthy();
//   const resultData = await response.json();
//   return resultData;
// };

// test('Check get list user successfully and incorrect incase get mock api', async ({ request }) => {
//   const resultData = await getListUser(request);
//   expect(resultData.length).toBe(9);
//   resultData.forEach(element => {
//     expect(element).toEqual(expect.objectContaining({
//       createdAt: expect.any(String), 
//       name: expect.any(String), 
//       avatar: expect.any(String),
//       age: expect.any(Number),
//       id: expect.any(String)
//     }));
//   });
// });

// test('Check create user successfully incase create by method: post', async({ request }) => {
//   const resultDataUser1 = await getListUser(request);
//   const lengthBeforeCreate = resultDataUser1.length;

//   const response = await request.post(url, {
//     data: {
//       name: 'Thuongne12',
//       avatar: 'http://thuong.com123',
//       age: 20
//     }
//   });
//   const resultDataNewUser = await response.json();

//   const resultDataUser2 = await getListUser(request);
//   const lenghtAfterCreate = resultDataUser2.length;
//   expect(lengthBeforeCreate + 1).toBe(lenghtAfterCreate);

//   expect(resultDataNewUser).toEqual(expect.objectContaining({
//     name: 'Thuongne12',
//     avatar: 'http://thuong.com123',
//     age: 20
//   }));
// });

// test('Check update user successfully incase update by method: PUT', async({ request }) => {
//   const resultDataUser1 = await getListUser(request);
//   const lengthBeforeUpdate = resultDataUser1.length;

//   const response = await request.put(url + `/9`, {
//     data: {
//       name: 'nguyenhoaithithuongday',
//       avatar: 'http://thuong.com',
//       age: 20
//     }
//   });
//   expect(response.ok()).toBeTruthy();
//   const resultDataUpdate = await response.json();

//   const resultDataUser2 = await getListUser(request);
//   const lenghtAfterUpdate = resultDataUser2.length;

//   const getUserId = await request.get(url + `/9`, {});
//   expect(getUserId.ok()).toBeTruthy();
//   const resultDataUserId = await getUserId.json();

//   expect(lengthBeforeUpdate).toBe(lenghtAfterUpdate);
//   expect(resultDataUserId).toEqual(expect.objectContaining(resultDataUpdate));
// });

// test.only('Check delete user successfully incase delete by method: DELETE', async({ request }) => {
//   const resultDataUser = await getListUser(request);
//   const lengthBeforeDelete = resultDataUser.length;

//   const response = await request.delete(url + `/7`, {});
//   expect(response.ok()).toBeTruthy();

//   const resultDataUser2 = await getListUser(request);
//   const lengthAfterDelete = resultDataUser2.length;
//   expect(lengthBeforeDelete - 1).toBe(lengthAfterDelete);

//   //Kiểm tra khi get user không tồn tại thì báo lỗi
//   const getUserId = await request.get(url + `/7`, {});
//   expect(getUserId.ok()).not.toBeTruthy();
// });


//const { test, expect } = require('@playwright/test');
// test("demo", async({page}) => {
//     await page.goto('https://playwright.dev/');
//     await page.getByText('Get started').click()
//     await expect(page).toHaveURL('https://playwright.dev/docs/intro')
// })


// test.only("login test case", async({page}) => {
//     //navigete
//     await page.goto('https://dev-tiktok.ecomdy.com/');

//     //login step
//     await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
//     await page.getByPlaceholder('Password').fill('12341234');
//     await page.locator('xpath=//*[@id="btn-login"]').click();

//     //verify
//     await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();

//     await page.locator('xpath=//*[@id="trigger-paypal-add-fund-child"]').click()
//     await page.locator('xpath=//*[@id="__BVID__174"]').fill('12');
//     //await page.locator('xpath=//*[@id="paypal-button-container"]').click();
//     //await expect(page.locator('xpath=//*[contains(@class,"text-danger")]')).toContainText('Amount is invalid (min: $200, max: $100,000)');
//     await expect(page.getByText('Amount is invalid (min: $200, max: $100,000)')).toBeVisible();
// })

// test("login test case", async({page}) => {
//     //navigete
//     await page.goto('https://dev-tiktok.ecomdy.com/');

//     //login step
//     await page.locator('xpath=//*[@id="login-email"]').type("nguyentu007979@gmail.com");
//     await page.locator('xpath=//*[@id="login-password"]').type("12341234");
//     await page.locator('xpath=//*[@id="btn-login"]').click();
//     //await page.waitForURL('https://dev-tiktok.ecomdy.com/update-information');

//     //verify
//     await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();
// })




    