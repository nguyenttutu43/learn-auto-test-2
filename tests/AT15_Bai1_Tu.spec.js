const { test, expect } = require('@playwright/test');
const { clear } = require('console');
const { verify } = require('crypto');

test.describe('test credit card', async() => {
    test('TC-166 Check top up successfully incase input amount to equal 200$', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');
        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.getByTestId('btn-login').click();
        //verify login
        await expect(page.getByTestId('btn-info-user')).toBeVisible();
        //Back to step 4
        await page.getByTestId('title-step-run-campaign-1').click();
        //
        const openingBalance = await page.locator('.amount.font-20.font-weight-bold').textContent();
        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()

        //input amount to equal 200$
        await page.getByTestId('input-amount-add-fund').fill('200');

        //verify display message pass
        //await expect(page.getByText('This amount is suitable')).toBeVisible();
        const error = page.locator('#trigger-credit-card-add-fund').locator('#amount').locator('//small');
        const messagePass = await error.textContent();
        expect(messagePass).toBe(' This amount is suitable ');

        //getByTestId('amount-add-fund')
        //verify enable button [Top up]
        await expect(page.getByTestId('trigger-top-up-cc').first()).not.toBeDisabled();
        //verify servicecharge, taxfee, total amount...
        await expect(page.locator('.summary-container').getByTestId('amount-credit-card')).toHaveText('200 USD');
        await expect(page.getByTestId('service-charge-credit-card')).toHaveText('6 USD');
        await expect(page.locator('.summary-container').getByTestId('credit-card-fee')).toHaveText('8 USD');
        await expect(page.getByTestId('tax-fee-credit-card')).toHaveText('6 USD');
        await expect(page.locator('.summary-container').getByTestId('total-amount-credit-card')).toHaveText('220 USD');

        await page.getByTestId('trigger-top-up-cc').first().click();
        //await page.waitForTimeout(30000);
        //verify top up successfully
        await expect(page.locator('.toastification-title.text-success')).toHaveText("Top up success!");
        //verify redirect to step 5 successfully
        await expect(page.locator('.content-header-title.float-left.pr-1.mb-0.border-right-0')).toHaveText('Get TikTok Agency Ads Account')
        //verify Closing Ecomdy Balance = Opening Ecomdy Balance + Amount
        const closingBalance = (Number(openingBalance) + 200).toString();
        expect(await page.locator('.amount.font-20.font-weight-bold').textContent()).toBe(closingBalance);
})

    test('Check top up failed incase no input amount', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');
        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.getByTestId('btn-login').click();
        //verify login
        await expect(page.getByTestId('btn-info-user')).toBeVisible();
        //Back to step 4
        await page.getByTestId('title-step-run-campaign-1').click();
        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()

        //input amount less than 200$
        await page.getByTestId('input-amount-add-fund').fill('15');
        await page.getByTestId('input-amount-add-fund').clear();

        //verify display message error
        await expect(page.getByText('The amount field is required')).toBeVisible();

        //verify disable button [Top up]
        await expect(page.locator('#trigger-top-up-cc').first()).toBeDisabled();
    })

    test('Check top up failed incase input amount less than 200$', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');

        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.locator('#btn-login').click();

        //verify login
        await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();

        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()
        //input amount less than 200$
        await page.locator('xpath=//*[@id="trigger-credit-card-add-fund"]//span[@id="amount"]//input').fill('100');

        //verify display message error
        await expect(page.getByText('Amount is invalid (min: $200, max: $100,000)')).toBeVisible();

        //verify disable button [Top up]
        await expect(page.locator('#trigger-top-up-cc').first()).toBeDisabled();
    })

    test('Check top up failed incase input amount greater than 100,000$', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');

        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.locator('#btn-login').click();

        //verify login
        await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();

        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()
        //input amount greater than 100,000$
        await page.locator('xpath=//*[@id="trigger-credit-card-add-fund"]//span[@id="amount"]//input').fill('100009');

        //verify display message error
        await expect(page.getByText('Amount is invalid (min: $200, max: $100,000)')).toBeVisible();

        //verify disable button [Top up]
        await expect(page.locator('#trigger-top-up-cc').first()).toBeDisabled();
    })

    test('Check top up failed incase input amount to equal 0', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');

        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.locator('#btn-login').click();

        //verify login
        await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();

        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()
        //input amount greater than 100,000$
        await page.locator('xpath=//*[@id="trigger-credit-card-add-fund"]//span[@id="amount"]//input').fill('0');

        //verify display message error
        await expect(page.getByText('Value must be greater than zero')).toBeVisible();

        //verify disable button [Top up]
        await expect(page.locator('#trigger-top-up-cc').first()).toBeDisabled();
    })
    //negative number

    test('Check top up failed incase input amount is negative number', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');

        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.locator('#btn-login').click();

        //verify login
        await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();

        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()
        //input amount is negative number
        await page.locator('xpath=//*[@id="trigger-credit-card-add-fund"]//span[@id="amount"]//input').fill('-200');

        //verify display message error
        await expect(page.getByText('Value must be greater than zero')).toBeVisible();

        //verify disable button [Top up]
        await expect(page.locator('#trigger-top-up-cc').first()).toBeDisabled();
    })

    test('Check top up failed incase input amount is text characters', async({page}) => {
        //navigete
        await page.goto('https://dev-tiktok.ecomdy.com/');

        //login step
        await page.getByPlaceholder('Enter your email').fill('nguyentu007979@gmail.com');
        await page.getByPlaceholder('Password').fill('12341234');
        await page.locator('#btn-login').click();

        //verify login
        await expect(page.locator('xpath=//*[contains(@class,"b-avatar-custom")]')).toBeVisible();

        //open credit card tab.
        await page.locator('#trigger-credit-card-add-fund-child').click()

        //verify not allow input text character.
        await expect(page.locator('xpath=//*[@id="trigger-credit-card-add-fund"]//span[@id="amount"]//input')).toHaveAttribute('type', 'number');

        //verify disable button [Top up]
        await expect(page.locator('#trigger-top-up-cc').first()).toBeDisabled();
    })
});