// const { test, expect } = require('@playwright/test');
// const { log } = require('console');
// const { request } = require('http');
// const { get } = require('https');
// const {name, age} = require('../utils/Check-reponse-utils')
// console.log({name, age});

// // test('Check create new campaign successfully incase input valid data into all the filed.', async({ request }) => {
// //     const getCampaignUrl = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const campaignPayload = {
// //         "budgetMode": "BUDGET_MODE_TOTAL",
// //         "budget": 50,
// //         "budgetOptimizeOn": false,
// //         "name": "TuNguyen1234",
// //         "objectiveType": "TRAFFIC"
// //     }

// //     const response = await request.post(getCampaignUrl, {
// //         data: campaignPayload
// //     });
// //     expect(response.ok()).toBeTruthy();

// //     const listCampaign = await request.get(getCampaignUrl)
// //     const resultCreate = await listCampaign.json()
// //     expect(resultCreate.result.content).toEqual(expect.arrayContaining([expect.objectContaining({ "name": "TuNguyen1234" })]));
// // });

// // test('Check create new campaign successfully incase input valid data into the required field', async({ request }) => {
// //     const getCampaignUrl = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const campaignPayload = {
// //         budgetMode: "BUDGET_MODE_INFINITE",
// //         budgetOptimizeOn: false,
// //         name: "NguyenTuTest",
// //         objectiveType: "TRAFFIC"
// //     }

// //     const response = await request.post(getCampaignUrl, {
// //         data: campaignPayload
// //     });
// //     expect(response.ok()).toBeTruthy();

// //     const listCampaign = await request.get(getCampaignUrl)
// //     const resultCreate = await listCampaign.json()
// //     expect(resultCreate.result.content).toEqual(expect.arrayContaining([expect.objectContaining({ "name": "NguyenTuTest" })]));

// // });

// // test('Check create new campaign unsuccessfully incase no input budgetMode', async({ request }) => {
// //     const getCampaignUrl = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const campaignPayload = {
// //         budgetMode: "",
// //         budgetOptimizeOn: false,
// //         name: "NguyenTuTest",
// //         objectiveType: "TRAFFIC"
// //     }

// //     const response = await request.post(getCampaignUrl, {
// //         data: campaignPayload
// //     });
// //     const result = await response.json();
// //     console.log(result.statusCode);
// //     expect(result.statusCode).toBe(400);
// //     expect(result.message).toEqual("Budget Mode must be one of the following values: BUDGET_MODE_DAY, BUDGET_MODE_TOTAL, BUDGET_MODE_INFINITE")
// // });

// // test('Check create new campaign unsuccessfully incase input text characters into budget field', async({ request }) => {
// //     const getCampaignUrl = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const campaignPayload = {
// //         budgetMode: "BUDGET_MODE_TOTAL",
// //         budget: "tunguyen",
// //         budgetOptimizeOn: false,
// //         name: "NguyenTuTest",
// //         objectiveType: "TRAFFIC"
// //     }

// //     const response = await request.post(getCampaignUrl, {
// //         data: campaignPayload
// //     });
// //     const result = await response.json();
// //     console.log(result.statusCode);
// //     expect(result.statusCode).toBe(400);
// //     expect(result.message).toEqual("Budget must be a number conforming to the specified constraints")
// // });

// // test('Check create new campaign unsuccessfully incase no input name', async({ request }) => {
// //     const getCampaignUrl = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const campaignPayload = {
// //         budgetMode: "BUDGET_MODE_TOTAL",
// //         budget: "tunguyen",
// //         budgetOptimizeOn: false,
// //         name: "",
// //         objectiveType: "TRAFFIC"
// //     }

// //     const response = await request.post(getCampaignUrl, {
// //         data: campaignPayload
// //     });
// //     const result = await response.json();
// //     console.log(result.statusCode);
// //     expect(result.statusCode).toBe(400);
// //     expect(result.message).toEqual("name: is not match with the validation")
// // });


// // test('Check get list user successfully', async({ request }) => {
// //     const getCampaignUrl = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const response = await request.get(getCampaignUrl);
// //     expect(response.ok()).toBeTruthy();

// //     const resultdata = await response.json();
// //     console.log(resultdata);
// // });

// // test('Check get draft campaign successfully incase user has draft campaign', async({ request }) => {
// //     const url = 'https://eac.ecomdy.com/api/eac/campaigns'
// //     const path = `?isDraft=true`
// //     const response = await request.get(url + path);
// //     expect(response.ok()).toBeTruthy();

// //     const resultdata = await response.json();
// //     if (resultdata.result.content.length > 0) {
// //         expect(resultdata.result.content).toEqual(expect.arrayContaining([expect.objectContaining({"isDraft": true})]))
// //     };
// // });

// test('Check get data successfully incase input path with limit anhd page', async({ request }) => {
//     const url = 'https://eac.ecomdy.com/api/eac/campaigns'
//     const path = `?limit=10&page=1`
//     const response = await request.get(url + path);
//     expect(response.ok()).toBeTruthy();

//     const resultdata = await response.json();
//     expect(resultdata.result.paging.page).toBe(1);
//     expect(resultdata.result.paging.limit).toBe(10)
// });


// // test('Check get campaign id successfully', async({ request }) => {
// //     const url = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const response = await request.get(url + `/651bc1bf6704cfe0686dc791`);
// //     expect(response.ok()).toBeTruthy();

// //     const resultdata = await response.json();
// //     //console.log(resultdata);
// //     expect(resultdata.result).toEqual(expect.objectContaining({"_id": "651bc1bf6704cfe0686dc791"}))
// // });

// // test('Check update campaign successfully incase input valid data the all field', async({ request }) => {
// //     const url = 'https://eac.ecomdy.com/api/eac/campaigns'

// //     const path = '/651bbc6b6704cfe0686656e4'

// //     const campaignPayload = {
// //         name: "NguyenTuTest",
// //         budgetMode: "BUDGET_MODE_TOTAL",
// //         budget: 50
// //     }

// //     const response = await request.patch(url + path, {
// //         data: campaignPayload
// //     });
// //     expect(response.ok()).toBeTruthy();

// //     const listCampaign = await request.get(url)

// //     const resultUpdate = await listCampaign.json()
// //     expect(resultUpdate.result.content).toEqual(expect.arrayContaining([expect.objectContaining({ "name": "NguyenTuTest" })]));

// // });

// // test('Check update campaign unsuccessfully incase no input budget', async({ request }) => {
// //     const url = 'https://eac.ecomdy.com/api/eac/campaigns'
// //     const path = '/652cde879e80a251eaeeea26'
// //     const campaignPayload = {
// //         name: "NguyenTuTest",
// //         budgetMode: "BUDGET_MODE_INFINITE",
// //         budget: "" 
// //     }

// //     const response = await request.patch(url + path, {
// //         data: campaignPayload
// //     });
// //     const result = await response.json();
// //     expect(result.statusCode).toBe(400);
// //     expect(result.message).toEqual("Budget must be a number conforming to the specified constraints")
// // });
