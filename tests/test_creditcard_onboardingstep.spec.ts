import { test } from '@playwright/test';
import { DemoLoginPage } from '../page/login_page'
import { CreditCardOnBoadingPage } from '../page/creditcard_onboardingstep_page'

test.beforeEach ('login', async ({page}) => {
    const demoLogin = new DemoLoginPage(page)
    await demoLogin.goto()
    await demoLogin.enterData()
    await demoLogin.clickOnLoginButton()
    await demoLogin.verifylogin()
})

test.describe('credit card', async () => {
    test('TC-166 Check top up successfully incase input amount to equal 200$', async({page}) => {
        const creditCard = new CreditCardOnBoadingPage(page);
        await creditCard.clickOnAddFundStep();
        const beforeBalance = await creditCard.beforeBalance();
        await creditCard.clickOnCreditCardTab();
        await creditCard.enterAmount();
        await creditCard.message();
        await creditCard.enableTopUpButton();
        await creditCard.summary();
        await creditCard.clickOnTopUpButton();
        await creditCard.topupSuccess();
        await creditCard.adAccountstep();
        const afterBalance = await creditCard.afterBalance();
        await creditCard.compareBalances ({beforeBalance, afterBalance})
})  
})